CREATE TABLE `shifts` (
  `idshifts` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`idshifts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `laboratories_shifts` (
  `idlaboratories_shifts` int(11) NOT NULL AUTO_INCREMENT,
  `id_shift` int(11) DEFAULT NULL,
  `id_laboratories` int(11) DEFAULT NULL,
  `start_day_of_week` int(11) DEFAULT NULL,
  `end_day_of_week` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlaboratories_shifts`),
  KEY `idshifts_FK_idx` (`id_shift`),
  CONSTRAINT `idshifts_laboratories_FK` FOREIGN KEY (`id_shift`) REFERENCES `shifts` (`idshifts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `consultories_shifts` (
  `idconsultories_shifts` int(11) NOT NULL AUTO_INCREMENT,
  `id_shift` int(11) DEFAULT NULL,
  `id_consultories` int(11) DEFAULT NULL,
  `start_day_of_week` int(11) DEFAULT NULL,
  `end_day_of_week` int(11) DEFAULT NULL,
  PRIMARY KEY (`idconsultories_shifts`),
  KEY `idshifts_FK_idx` (`id_shift`),
  CONSTRAINT `idshifts_FK` FOREIGN KEY (`id_shift`) REFERENCES `shifts` (`idshifts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;