package com.softtek.msacademy.hospital.shiftsapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.LaboratoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.LaboratoryPeriodRepository;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class LaboratoryPeriodServiceImpl implements LaboratoryPeriodService{

    @Autowired
    private LaboratoryPeriodRepository laboratoryPeriodRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    public LaboratoryPeriodServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "writeThreadPool")
    public LaboratoryPeriod createLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod) {
        return laboratoryPeriodRepository.save(laboratoryPeriod);
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "getThreadPool")
    public List<LaboratoryPeriod> getLaboratoryPeriods() {
        return laboratoryPeriodRepository.findAll();
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "deleteThreadPool")
    public LaboratoryPeriod deleteLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod) {
        laboratoryPeriodRepository.delete(laboratoryPeriod);

        return  laboratoryPeriod;
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "writeThreadPool")
    public LaboratoryPeriod updateLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod) {
        Optional<LaboratoryPeriod> periodOptional = laboratoryPeriodRepository.findById(laboratoryPeriod.getLaboratoryShiftId());

        LaboratoryPeriod periodToUpdate = periodOptional.get();

        periodToUpdate.setLaboratoryId(laboratoryPeriod.getLaboratoryId());
        periodToUpdate.setEndDayOfWeek(laboratoryPeriod.getEndDayOfWeek());
        periodToUpdate.setStartDayOfWeek(laboratoryPeriod.getStartDayOfWeek());
        periodToUpdate.setShift(laboratoryPeriod.getShift());

        return laboratoryPeriodRepository.save(periodToUpdate);
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "writeThreadPool")
    public LaboratoryPeriod patchLaboratoryPeriod(int id, int shiftId, int startDay, int endDay) {
        Optional<LaboratoryPeriod> periodOptional = laboratoryPeriodRepository.findById(id);

        LaboratoryPeriod periodToUpdate = periodOptional.get();

        periodToUpdate.setEndDayOfWeek(endDay);
        periodToUpdate.setStartDayOfWeek(startDay);
        periodToUpdate.setShift(shiftRepository.findById(shiftId).get());

        return laboratoryPeriodRepository.save(periodToUpdate);
    }

    @Override
    public boolean exists(int id) {
        return laboratoryPeriodRepository.existsById(id);
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "getThreadPool" )
    public List<LaboratoryPeriod> findForCheck(int idLaboratory, int idShift, int startDay, int endDay) {
        return laboratoryPeriodRepository.findForCheck(idLaboratory, shiftRepository.findById(idShift).get(), startDay, endDay);
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "getThreadPool" )
    public LaboratoryPeriod getLaboratoryPeriod(int id) {
        try {
            return laboratoryPeriodRepository.findById(id).get();
        } catch(Exception e){
            return null;
        }
    }

    @Override
    @HystrixCommand(
            threadPoolKey = "getThreadPool" )
    public List<LaboratoryPeriod> findPeriodsForLaboratory(Integer id) {
        return laboratoryPeriodRepository.findPeriodsForLaboratory(id);
    }

}
