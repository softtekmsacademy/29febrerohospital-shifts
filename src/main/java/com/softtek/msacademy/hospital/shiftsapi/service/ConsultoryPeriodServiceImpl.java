package com.softtek.msacademy.hospital.shiftsapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.ConsultoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.ConsultoryPeriodRepository;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@DefaultProperties(
    threadPoolProperties = {
        @HystrixProperty( name = "coreSize", value = "20" ),
        @HystrixProperty( name = "maxQueueSize", value = "10") },
    commandProperties = {
        @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
        @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
        @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
        @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
        @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
        @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class ConsultoryPeriodServiceImpl implements ConsultoryPeriodService {

    @Autowired
    private ConsultoryPeriodRepository consultoryPeriodRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    public ConsultoryPeriodServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool")
    public ConsultoryPeriod createConsultoryPeriod(ConsultoryPeriod consultoryPeriod) {
        return consultoryPeriodRepository.save(consultoryPeriod);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool")
    public List<ConsultoryPeriod> getConsultoryPeriods() {
        return consultoryPeriodRepository.findAll();
    }

    @Override
    @HystrixCommand(threadPoolKey = "deleteThreadPool")
    public ConsultoryPeriod deleteConsultoryPeriod(ConsultoryPeriod consultoryPeriod) {
        consultoryPeriodRepository.delete(consultoryPeriod);

        return  consultoryPeriod;
    }

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool")
    public ConsultoryPeriod updateConsultoryPeriod(ConsultoryPeriod consultoryPeriod) {
        Optional<ConsultoryPeriod> periodOptional = consultoryPeriodRepository.findById(consultoryPeriod.getConsultoryShiftId());

        ConsultoryPeriod periodToUpdate = periodOptional.get();

        periodToUpdate.setConsultoryId(consultoryPeriod.getConsultoryId());
        periodToUpdate.setEndDayOfWeek(consultoryPeriod.getEndDayOfWeek());
        periodToUpdate.setStartDayOfWeek(consultoryPeriod.getStartDayOfWeek());
        periodToUpdate.setShift(consultoryPeriod.getShift());

        return consultoryPeriodRepository.save(periodToUpdate);
    }

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool")
    public ConsultoryPeriod patchConsultoryPeriod(int id, int shiftId, int startDay, int endDay) {
        Optional<ConsultoryPeriod> periodOptional = consultoryPeriodRepository.findById(id);

        ConsultoryPeriod periodToUpdate = periodOptional.get();

        periodToUpdate.setEndDayOfWeek(endDay);
        periodToUpdate.setStartDayOfWeek(startDay);
        periodToUpdate.setShift(shiftRepository.findById(shiftId).get());

        return consultoryPeriodRepository.save(periodToUpdate);
    }

    @Override
    public boolean exists(int id) {
        return consultoryPeriodRepository.existsById(id);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public List<ConsultoryPeriod> findForCheck(int idConsultory, int idShift, int startDay, int endDay) {
        return consultoryPeriodRepository.findForCheck(idConsultory, shiftRepository.findById(idShift).get(), startDay, endDay);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public ConsultoryPeriod getConsultoryPeriod(int id) {
        try {
            return consultoryPeriodRepository.findById(id).get();
        } catch(Exception e){
            return null;
        }
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public List<ConsultoryPeriod> findPeriodsForConsultory(int id) {
        return consultoryPeriodRepository.findPeriodsForConsultory(id);
    }
}
