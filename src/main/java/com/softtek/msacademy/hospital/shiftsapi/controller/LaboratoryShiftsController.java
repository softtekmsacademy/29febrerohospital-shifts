package com.softtek.msacademy.hospital.shiftsapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.CustomError;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.LaboratoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.LaboratoryPeriodRepository;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.ShiftRepository;
import com.softtek.msacademy.hospital.shiftsapi.service.LaboratoryPeriodService;
import com.softtek.msacademy.hospital.shiftsapi.service.ShiftsService;
import com.softtek.msacademy.hospital.shiftsapi.service.WorkAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class LaboratoryShiftsController {

    private final ObjectMapper objectMapper;

    @Autowired
    private ShiftsService shiftsService;


    @Autowired
    private LaboratoryPeriodService laboratoryPeriodService;

    @Autowired
    private WorkAreasService workAreasService;


    @Autowired
    public LaboratoryShiftsController(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @PostMapping(path = "/laboratories/{laboratoryId}/periods",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> postLaboratoryShift(@PathVariable(value = "laboratoryId") Integer laboratoryId, @Valid @RequestBody LaboratoryPeriod laboratoryPeriod) {
        laboratoryPeriod.setLaboratoryId(laboratoryId);
        try {
            if(!workAreasService.laboratoryExists(laboratoryId)){//Calls workareas service to check if laboratory exists
                return new ResponseEntity<>(objectMapper.writeValueAsString(new CustomError(HttpStatus.NOT_FOUND.value(),
                        "Laboratory with ID " + laboratoryId + " doesn't exist")),HttpStatus.NOT_FOUND);
            }
            List<LaboratoryPeriod> aux = laboratoryPeriodService.findForCheck(laboratoryId,
                    laboratoryPeriod.getShift().getShiftId(),laboratoryPeriod.getStartDayOfWeek(),
                    laboratoryPeriod.getEndDayOfWeek());
            if(aux.size()>0){
                return new ResponseEntity<>("This working period has already been assigned.",HttpStatus.CONFLICT);
            }
            LaboratoryPeriod create = laboratoryPeriodService.createLaboratoryPeriod(laboratoryPeriod);

            String arrayToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(arrayToJson, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Shift does not exist in database",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/laboratories/{laboratoryId}/periods",
            produces = "application/json")
    public ResponseEntity<String> getLaboratoryPeriods(@PathVariable(value = "laboratoryId") Integer laboratoryId){
        try{
            List<LaboratoryPeriod> laboratoryPeriodsList = new ArrayList<>();

            laboratoryPeriodsList = laboratoryPeriodService.findPeriodsForLaboratory(laboratoryId);


            String arrayToJson = objectMapper.writeValueAsString(laboratoryPeriodsList);
            return new ResponseEntity<>(arrayToJson, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "laboratories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateLaboratoryPeriod(@PathVariable(value = "periodId") Integer periodId, @Valid @RequestBody LaboratoryPeriod laboratoryPeriod){
        laboratoryPeriod.setLaboratoryShiftId(periodId);
        try{

            LaboratoryPeriod period = laboratoryPeriodService.getLaboratoryPeriod(periodId);

            if(period == null) {
                return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
            }

            if(!workAreasService.laboratoryExists(laboratoryPeriod.getLaboratoryId())){//Calls workareas service to check if laboratory exists
                return new ResponseEntity<>(objectMapper.writeValueAsString(new CustomError(HttpStatus.NOT_FOUND.value(),
                        "Laboratory with ID " + laboratoryPeriod.getLaboratoryId() + " doesn't exist")),HttpStatus.NOT_FOUND);
            }

            period.setEndDayOfWeek(laboratoryPeriod.getEndDayOfWeek());
            period.setStartDayOfWeek(laboratoryPeriod.getStartDayOfWeek());
            period.setLaboratoryId(laboratoryPeriod.getLaboratoryId());
            period.setShift(laboratoryPeriod.getShift());

            List<LaboratoryPeriod> aux = laboratoryPeriodService.findForCheck(period.getLaboratoryId(),
                    period.getShift().getShiftId(), period.getStartDayOfWeek(),period.getEndDayOfWeek());
            if(aux.size()>0){
                return new ResponseEntity<>("This working period has already been assigned.",HttpStatus.CONFLICT);
            }

            LaboratoryPeriod create = laboratoryPeriodService.createLaboratoryPeriod(period);

            String arrayToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(arrayToJson, HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(path = "laboratories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> deleteLaboratoryPeriod(@PathVariable(value = "periodId") Integer periodId){
        try{

            LaboratoryPeriod period = laboratoryPeriodService.getLaboratoryPeriod(periodId);

            if(period == null) {
                return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
            }

            LaboratoryPeriod deleted = laboratoryPeriodService.deleteLaboratoryPeriod(period);

            String arrayToJson = objectMapper.writeValueAsString(period);
            return new ResponseEntity<>(arrayToJson + "\n\nDELETED", HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }
    }

    @PatchMapping(path = "/laboratories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateName(@PathVariable(value = "periodId") Integer periodId,
                                             @RequestParam(value = "shift",required = false) Integer shiftId,
                                             @RequestParam(value = "startDay",required = false) Integer startDay,
                                             @RequestParam(value = "endDay",required = false) Integer endDay){
        LaboratoryPeriod period = laboratoryPeriodService.getLaboratoryPeriod(periodId);

        if(period == null) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }

        try {
            if(shiftId!=null) {
                period.setShift(shiftsService.getShift(shiftId));
                if(period.getShift()==null){
                    return new ResponseEntity<>("The selected ID for Shift assignment does not exist in database",
                            HttpStatus.NOT_FOUND);
                }
            }
            if(startDay!=null) {
                period.setStartDayOfWeek(startDay);
            }
            if(endDay!=null) {
                period.setEndDayOfWeek(endDay);
            }


            List<LaboratoryPeriod> aux = laboratoryPeriodService.findForCheck(period.getLaboratoryId(),
                    period.getShift().getShiftId(), period.getStartDayOfWeek(),period.getEndDayOfWeek());

            if(aux.size()>0){
                return new ResponseEntity<>("This working period has already been assigned.",HttpStatus.CONFLICT);
            }
            LaboratoryPeriod create = laboratoryPeriodService.createLaboratoryPeriod(period);

            String objectToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(objectToJson,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
