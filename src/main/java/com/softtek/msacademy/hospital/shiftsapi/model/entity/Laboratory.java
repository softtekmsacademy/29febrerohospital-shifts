package com.softtek.msacademy.hospital.shiftsapi.model.entity;

public class Laboratory {

    private int id;
    private String name;

    public Laboratory() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
