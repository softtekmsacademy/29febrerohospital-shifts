package com.softtek.msacademy.hospital.shiftsapi.service;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;

import java.time.LocalTime;
import java.util.List;

public interface ShiftsService {

    Shift createShift(Shift shift);

    List<Shift> getShifts();

    Shift deleteShift(Shift shift);

    Shift updateShift(Shift shift);

    Shift patchShift(int id, String name, int startHour, int startMin, int startSec, int endHour, int endMin, int endSec);

    boolean exists(int idShift);

    List<Shift> findForCheck(LocalTime startTime, LocalTime endTime);

    Shift getShift(int shiftId);
}
