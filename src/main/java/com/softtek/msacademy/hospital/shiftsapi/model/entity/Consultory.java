package com.softtek.msacademy.hospital.shiftsapi.model.entity;

import java.time.LocalTime;

public class Consultory {

    private int id;
    private String name;
    private LocalTime estimated_duration;

    public Consultory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getEstimated_duration() {
        return estimated_duration;
    }

    public void setEstimated_duration(LocalTime estimated_duration) {
        this.estimated_duration = estimated_duration;
    }
}
