package com.softtek.msacademy.hospital.shiftsapi.model.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity (name = "shifts")
@Table(name = "shifts")
public class Shift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idshifts")
    private Integer shiftId;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    @NotNull
    private LocalTime endTime;

    public Shift() {
    }

    public Shift(Integer idshifts, String name, LocalTime start_time, LocalTime end_time) {
        this.shiftId = idshifts;
        this.name = name;
        this.startTime = start_time;
        this.endTime = end_time;
    }

    public Integer getShiftId() {
        return shiftId;
    }

    public void setShiftId(Integer idshifts) {
        this.shiftId = idshifts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime start_time) {
        this.startTime = start_time;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime end_time) {
        this.endTime = end_time;
    }
}
