package com.softtek.msacademy.hospital.shiftsapi.model.repository;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;

@Repository
public interface ShiftRepository extends JpaRepository<Shift,Integer> {
    @Query("SELECT s FROM shifts s where s.startTime = :startTime and s.endTime = :endTime")
    List<Shift> findForCheck(@Param("startTime") LocalTime startTime, @Param("endTime") LocalTime endTime);

}
