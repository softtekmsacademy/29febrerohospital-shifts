package com.softtek.msacademy.hospital.shiftsapi.service;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.LaboratoryPeriod;

import java.util.List;

public interface LaboratoryPeriodService {

    LaboratoryPeriod createLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod);

    List<LaboratoryPeriod> getLaboratoryPeriods();

    LaboratoryPeriod deleteLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod);

    LaboratoryPeriod updateLaboratoryPeriod(LaboratoryPeriod laboratoryPeriod);

    LaboratoryPeriod patchLaboratoryPeriod(int id, int shiftId, int startDay, int endDay);

    boolean exists(int id);

    List<LaboratoryPeriod> findForCheck(int idLaboratory, int idShift, int startDay, int endDay);

    LaboratoryPeriod getLaboratoryPeriod(int id);

    List<LaboratoryPeriod> findPeriodsForLaboratory(Integer id);
}
