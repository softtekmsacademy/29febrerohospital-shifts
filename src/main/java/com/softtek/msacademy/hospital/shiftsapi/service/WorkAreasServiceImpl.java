package com.softtek.msacademy.hospital.shiftsapi.service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Consultory;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Laboratory;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class WorkAreasServiceImpl implements WorkAreasService {

    private final RestTemplate call = new RestTemplate();

    @Autowired
    private EurekaClient discoveryClient;

    @Override
    @HystrixCommand(threadPoolKey = "verifyThreadPool",
            fallbackMethod = "error")
    public boolean consultoryExists(int idConsultory) {
        String url=getServiceUrlNoPort() + "/consultories/" + idConsultory;
        Consultory response = call.getForObject(url, Consultory.class);

        if(response != null) {
            return response.getId() != 0;
        }

        return false;
    }

    @Override
    @HystrixCommand(threadPoolKey = "verifyThreadPool",
            fallbackMethod = "error" )
    public boolean laboratoryExists(int idLaboratory) {
        String url=getServiceUrlNoPort() + "/laboratories/" + idLaboratory;
        Laboratory response = call.getForObject(url,Laboratory.class);

        if(response != null) {
            return response.getId() != 0;
        }

        return false;
    }

    boolean error(int id) {
        return false;
    }

    private String getServiceUrlNoPort(){
        String urlNoPort="";
        List<Application> applications = discoveryClient.getApplications().getRegisteredApplications();

        for (Application application : applications) {
            List<InstanceInfo> applicationsInstances = application.getInstances();
            for (InstanceInfo applicationsInstance : applicationsInstances) {
                String name = applicationsInstance.getAppName();
                String url = applicationsInstance.getHomePageUrl();
                if(name.equals("WORKAREAS-API") && url.contains("herokuapp")){
                    urlNoPort = applicationsInstance.getHostName();
                }
            }
        }
        return ("https://" + urlNoPort);
    }
}
