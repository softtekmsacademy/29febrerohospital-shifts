package com.softtek.msacademy.hospital.shiftsapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import com.softtek.msacademy.hospital.shiftsapi.service.ShiftsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalTime;
import java.util.List;

@RestController
public class ShiftsController {

    @Autowired
    private ShiftsService shiftsService;

    private final ObjectMapper objectMapper;

    @Autowired
    public ShiftsController(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @PostMapping(path = "/shifts",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> createShift(@Valid @RequestBody Shift shift){
        try {
            List<Shift> checkExistence = shiftsService.findForCheck(shift.getStartTime(),shift.getEndTime());
            if(checkExistence.size()>0){ //Checks that there are no Shift in Database with the startTime and endTime received
                return new ResponseEntity<>("Shift with sent startTime and endTime already exists.",HttpStatus.CONFLICT);
            }
            else {
                if(shift.getStartTime().isAfter(shift.getEndTime())){ //Checks if startTime is a time value before endTime, works otherwise too.
                    return new ResponseEntity<String>("startTime and endTime are incorrect", HttpStatus.BAD_REQUEST);
                }
                Shift created = shiftsService.createShift(shift);
                String objectToJson = objectMapper.writeValueAsString(created);
                return new ResponseEntity<>(objectToJson, HttpStatus.CREATED);
            }
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @GetMapping(path = "/shifts",
            produces = "application/json")
    public ResponseEntity<String> getShifts() throws InterruptedException {
        List<Shift> shiftList = shiftsService.getShifts();
        try {
            String arrayToJson = objectMapper.writeValueAsString(shiftList);
            return new ResponseEntity<>(arrayToJson, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(path = "/shifts/{shiftId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> deleteShift(@PathVariable(value = "shiftId") Integer shiftId){
        Shift shiftToDelete = shiftsService.getShift(shiftId);

        if(shiftToDelete!=null){
            try {
                String objectToJson = objectMapper.writeValueAsString(shiftToDelete);
                shiftsService.deleteShift(shiftToDelete);
                return new ResponseEntity<String>(objectToJson + "DELETED",HttpStatus.OK);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/shifts/{shiftId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateShift(@PathVariable(value = "shiftId") Integer shiftId, @Valid @RequestBody Shift shift){
        Shift shiftToUpdate = shiftsService.getShift(shiftId);

        if(shiftToUpdate!=null){
            try {
                if(shiftsService.findForCheck(shift.getStartTime(),shift.getEndTime()).size()>0){ //Checks that there are no Shift in Database with the startTime and endTime received
                    return new ResponseEntity<>("Shift with sent startTime and endTime already exists.",HttpStatus.CONFLICT);
                }
                else {
                    shiftToUpdate.setName(shift.getName());
                    shiftToUpdate.setStartTime(shift.getStartTime());
                    shiftToUpdate.setEndTime(shift.getEndTime());
                    if(shiftToUpdate.getStartTime().isAfter(shiftToUpdate.getEndTime())){//Checks if startTime is a time value before endTime, works otherwise too.
                        return new ResponseEntity<String>("startTime and endTime are incorrect", HttpStatus.BAD_REQUEST);
                    }
                    Shift create = shiftsService.createShift(shiftToUpdate);
                    String objectToJson = objectMapper.writeValueAsString(create);
                    return new ResponseEntity<String>(objectToJson, HttpStatus.OK);
                }
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(path =  "/shifts/{shiftId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateName(@PathVariable(value = "shiftId") Integer shiftId,
                                             @RequestParam(value = "name",required = false) String shiftName,
                                             @RequestParam(value = "startHour",required = false) Integer shiftStartHour,
                                             @RequestParam(value = "startMin",required = false) Integer shiftStartMin,
                                             @RequestParam(value = "startSec",required = false) Integer shiftStartSec,
                                             @RequestParam(value = "endHour",required = false) Integer shiftEndHour,
                                             @RequestParam(value = "endMin",required = false) Integer shiftEndMin,
                                             @RequestParam(value = "endSec",required = false) Integer shiftEndSec){
        Boolean timeChange = false;
        Shift shiftToUpdate = shiftsService.getShift(shiftId);

        if(shiftToUpdate!=null){
            try {
                if(shiftName!=null) {
                    shiftToUpdate.setName(shiftName);
                }
                if(shiftStartHour!=null&&shiftStartMin!=null&&shiftStartSec!=null) {
                    shiftToUpdate.setStartTime(LocalTime.of(shiftStartHour, shiftStartMin, shiftStartSec));
                    timeChange = true;
                }
                if(shiftEndHour!=null&&shiftEndMin!=null&&shiftEndSec!=null) {
                    shiftToUpdate.setEndTime(LocalTime.of(shiftEndHour, shiftEndMin, shiftEndSec));
                    timeChange = true;
                }
                if(timeChange && shiftsService.findForCheck(shiftToUpdate.getStartTime(),shiftToUpdate.getEndTime()).size()>0){//Checks that there are no Shift in Database with the startTime and endTime received
                    return new ResponseEntity<>("Shift with sent startTime and endTime already exists.",HttpStatus.CONFLICT);
                }
                if(shiftToUpdate.getStartTime().isAfter(shiftToUpdate.getEndTime())){//Checks if startTime is a time value before endTime, works otherwise too.
                    return new ResponseEntity<String>("startTime and endTime are incorrect", HttpStatus.BAD_REQUEST);
                }
                Shift create = shiftsService.createShift(shiftToUpdate);
                String objectToJson = objectMapper.writeValueAsString(create);
                return new ResponseEntity<>(objectToJson,HttpStatus.OK);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
