package com.softtek.msacademy.hospital.shiftsapi.model.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "laboratories_shifts")
@Table(name = "laboratories_shifts")
public class LaboratoryPeriod {

    @Id
    @Column(name = "idlaboratories_shifts")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer laboratoryShiftId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_shift")
    private Shift shift;

    @Column(name = "id_laboratories")
    private Integer laboratoryId;

    @NotNull
    @Column(name = "start_day_of_week")
    private Integer startDayOfWeek;

    @NotNull
    @Column(name = "end_day_of_week")
    private Integer endDayOfWeek;

    public LaboratoryPeriod() {
    }

    public LaboratoryPeriod(Integer idlaboratories_shifts, Shift id_shifts, Integer id_laboratories, Integer start_day_of_week, Integer end_day_of_week) {
        this.laboratoryShiftId = idlaboratories_shifts;
        this.shift = id_shifts;
        this.laboratoryId = id_laboratories;
        this.startDayOfWeek = start_day_of_week;
        this.endDayOfWeek = end_day_of_week;
    }

    public Integer getLaboratoryShiftId() {
        return laboratoryShiftId;
    }

    public void setLaboratoryShiftId(Integer idlaboratories_shifts) {
        this.laboratoryShiftId = idlaboratories_shifts;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift id_shifts) {
        this.shift = id_shifts;
    }

    public Integer getLaboratoryId() {
        return laboratoryId;
    }

    public void setLaboratoryId(Integer id_laboratories) {
        this.laboratoryId = id_laboratories;
    }

    public Integer getStartDayOfWeek() {
        return startDayOfWeek;
    }

    public void setStartDayOfWeek(Integer start_day_of_week) {
        this.startDayOfWeek = start_day_of_week;
    }

    public Integer getEndDayOfWeek() {
        return endDayOfWeek;
    }

    public void setEndDayOfWeek(Integer end_day_of_week) {
        this.endDayOfWeek = end_day_of_week;
    }
}
