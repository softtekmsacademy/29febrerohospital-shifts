package com.softtek.msacademy.hospital.shiftsapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.ConsultoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.service.ConsultoryPeriodService;
import com.softtek.msacademy.hospital.shiftsapi.service.ShiftsService;
import com.softtek.msacademy.hospital.shiftsapi.service.WorkAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ConsultoryShiftsController {

    @Autowired
    private ConsultoryPeriodService consultoryPeriodService;

    @Autowired
    private ShiftsService shiftsService;

    @Autowired
    private WorkAreasService workAreasService;

    private final ObjectMapper objectMapper;

    @Autowired
    public ConsultoryShiftsController(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @PostMapping(path = "/consultories/{consultoryId}/periods",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> postConsultoryShift(@PathVariable(value = "consultoryId") Integer consultoryId, @Valid @RequestBody ConsultoryPeriod consultoryPeriod){

        if(!workAreasService.consultoryExists(consultoryId)) {
            return new ResponseEntity<>("The selected consultory does not exist.", HttpStatus.NOT_FOUND);
        }

        consultoryPeriod.setConsultoryId(consultoryId);
        List<ConsultoryPeriod> aux = consultoryPeriodService
                .findForCheck(consultoryId, consultoryPeriod.getShift().getShiftId(),
                              consultoryPeriod.getStartDayOfWeek(), consultoryPeriod.getEndDayOfWeek());
        try {
            if(aux.size() > 0) {
                return new ResponseEntity<>("This working period has already been assigned.", HttpStatus.CONFLICT);
            }
            ConsultoryPeriod create = consultoryPeriodService.createConsultoryPeriod(consultoryPeriod);
            String objectToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(objectToJson, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("This working period have already been assigned.",HttpStatus.CONFLICT);
        }
    }

    @GetMapping(path = "/consultories/{consultoryId}/periods",
            produces = "application/json")
    public ResponseEntity<String> getConsultoryPeriods(@PathVariable(value = "consultoryId") Integer consultoryId){
        try{
            List<ConsultoryPeriod> consultoryPeriodList = new ArrayList<>();

            consultoryPeriodList = consultoryPeriodService.findPeriodsForConsultory(consultoryId);

            String arrayToJson = objectMapper.writeValueAsString(consultoryPeriodList);
            return new ResponseEntity<>(arrayToJson, HttpStatus.OK);

        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "consultories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateConsultoryPeriod(@PathVariable(value = "periodId") Integer periodId, @Valid @RequestBody ConsultoryPeriod consultoryPeriod){

        if(!workAreasService.consultoryExists(consultoryPeriod.getConsultoryId())) {
            return new ResponseEntity<>("The selected consultory does not exist.", HttpStatus.NOT_FOUND);
        }

        consultoryPeriod.setConsultoryShiftId(periodId);
        try{
            ConsultoryPeriod period = consultoryPeriodService.getConsultoryPeriod(periodId);

            if(period == null) {
                return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
            }

            period.setEndDayOfWeek(consultoryPeriod.getEndDayOfWeek());
            period.setStartDayOfWeek(consultoryPeriod.getStartDayOfWeek());
            period.setConsultoryId(consultoryPeriod.getConsultoryId());
            period.setShift(consultoryPeriod.getShift());

            List<ConsultoryPeriod> aux = consultoryPeriodService.findForCheck(period.getConsultoryId(), period.getShift().getShiftId(),
                    period.getStartDayOfWeek(), period.getEndDayOfWeek());

            if(aux.size() > 0) {
                return new ResponseEntity<>("This working period has already been assigned", HttpStatus.CONFLICT);
            }

            ConsultoryPeriod create = consultoryPeriodService.createConsultoryPeriod(period);

            String objectToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(objectToJson, HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(path = "consultories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> deleteConsultoryPeriod(@PathVariable(value = "periodId") Integer periodId){
        try{
            ConsultoryPeriod period = consultoryPeriodService.getConsultoryPeriod(periodId);

            if(period == null) {
                return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
            }

            ConsultoryPeriod deleted = consultoryPeriodService.deleteConsultoryPeriod(period);

            String objectToJson = objectMapper.writeValueAsString(deleted);
            return new ResponseEntity<>(objectToJson + "\n\nDELETED", HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }
    }

    @PatchMapping(path = "/consultories/periods/{periodId}",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> updateName(@PathVariable(value = "periodId") Integer periodId,
                                             @RequestParam(value = "shift",required = false) Integer shiftId,
                                             @RequestParam(value = "startDay",required = false) Integer startDay,
                                             @RequestParam(value = "endDay",required = false) Integer endDay){
        ConsultoryPeriod period = consultoryPeriodService.getConsultoryPeriod(periodId);

        if(period == null) {
            return new ResponseEntity<>("The working period does not exist.", HttpStatus.NO_CONTENT);
        }

        try {
            if(shiftId != null) {
                period.setShift(shiftsService.getShift(shiftId));
                if(period.getShift() == null) {
                    return new ResponseEntity<>("The selected ID for Shift assignment does not exist in database.", HttpStatus.NOT_FOUND);
                }
            }
            if(startDay != null) {
                period.setStartDayOfWeek(startDay);
            }
            if(endDay != null) {
                period.setEndDayOfWeek(endDay);
            }

            List<ConsultoryPeriod> aux = consultoryPeriodService.findForCheck(period.getConsultoryId(), period.getShift().getShiftId(),
                    period.getStartDayOfWeek(), period.getEndDayOfWeek());
            if(aux.size() > 0) {
                return new ResponseEntity<>("This working period has already been assigned.", HttpStatus.CONFLICT);
            }
            ConsultoryPeriod create = consultoryPeriodService.createConsultoryPeriod(period);
            String objectToJson = objectMapper.writeValueAsString(create);
            return new ResponseEntity<>(objectToJson, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
