package com.softtek.msacademy.hospital.shiftsapi.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import com.softtek.msacademy.hospital.shiftsapi.model.repository.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class ShiftsServiceImpl implements ShiftsService {

    @Autowired
    private ShiftRepository shiftRepository;

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool" )
    public Shift createShift(Shift shift) {
        return shiftRepository.save(shift);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public List<Shift> getShifts() {
        return shiftRepository.findAll();
    }

    @Override
    @HystrixCommand(threadPoolKey = "deleteThreadPool" )
    public Shift deleteShift(Shift shift) {
        shiftRepository.delete(shift);

        return shift;
    }

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool" )
    public Shift updateShift(Shift shift) {
        Optional<Shift> shiftOptional = shiftRepository.findById(shift.getShiftId());

        Shift shiftToUpdate = shiftOptional.get();

        shiftToUpdate.setName(shift.getName());
        shiftToUpdate.setEndTime(shift.getEndTime());
        shiftToUpdate.setStartTime(shift.getStartTime());

        return shiftRepository.save(shiftToUpdate);
    }

    @Override
    @HystrixCommand(threadPoolKey = "writeThreadPool" )
    public Shift patchShift(int id, String name, int startHour, int startMin, int startSec, int endHour, int endMin, int endSec) {
        Optional<Shift> shiftOptional = shiftRepository.findById(id);

        Shift shiftToUpdate = shiftOptional.get();

        shiftToUpdate.setName(name);
        shiftToUpdate.setStartTime(LocalTime.of(startHour,startMin,startSec));
        shiftToUpdate.setEndTime(LocalTime.of(endHour,endMin,endSec));

        return shiftRepository.save(shiftToUpdate);
    }

    @Override
    public boolean exists(int idShift) {
        return shiftRepository.existsById(idShift);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public List<Shift> findForCheck(LocalTime startTime, LocalTime endTime) {
        return shiftRepository.findForCheck(startTime, endTime);
    }

    @Override
    @HystrixCommand(threadPoolKey = "getThreadPool" )
    public Shift getShift(int shiftId) {
        try {
            return shiftRepository.findById(shiftId).get();
        } catch(Exception e){
            return null;
        }
    }
}