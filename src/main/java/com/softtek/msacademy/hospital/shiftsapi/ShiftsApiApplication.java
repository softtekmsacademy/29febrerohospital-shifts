package com.softtek.msacademy.hospital.shiftsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@SpringBootApplication
@EnableCircuitBreaker
@EnableDiscoveryClient
public class ShiftsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShiftsApiApplication.class, args);
	}

}
