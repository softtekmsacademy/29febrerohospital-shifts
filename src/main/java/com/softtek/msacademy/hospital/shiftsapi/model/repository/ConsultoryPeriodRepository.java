package com.softtek.msacademy.hospital.shiftsapi.model.repository;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.ConsultoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultoryPeriodRepository extends JpaRepository<ConsultoryPeriod,Integer> {
    @Query("SELECT s FROM consultories_shifts s where s.consultoryId = :idconsultories")
    List<ConsultoryPeriod> findPeriodsForConsultory(@Param("idconsultories") Integer idconsultories);

    @Query("SELECT s FROM consultories_shifts s where s.consultoryId = :idconsultories and s.shift = :idshift and s.startDayOfWeek = :startday " +
            "and s.endDayOfWeek = :endday")
    List<ConsultoryPeriod> findForCheck(@Param("idconsultories") Integer idconsultories, @Param("idshift") Shift idshift, @Param("startday") Integer startday, @Param("endday") Integer endday);
}
