package com.softtek.msacademy.hospital.shiftsapi.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "consultories_shifts")
@Table(name = "consultories_shifts")
public class ConsultoryPeriod {
    @Id
    @Column(name = "idconsultories_shifts")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer consultoryShiftId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_shift")
    private Shift shift;

    @Column(name = "id_consultories")
    private Integer consultoryId;

    @NotNull
    @Column(name = "start_day_of_week")
    private Integer startDayOfWeek;

    @NotNull
    @Column(name = "end_day_of_week")
    private Integer endDayOfWeek;

    public ConsultoryPeriod() {
    }

    public ConsultoryPeriod(Integer consultoryShiftId, Shift id_shifts, Integer id_consultories, Integer start_day_of_week, Integer end_day_of_week) {
        this.consultoryShiftId = consultoryShiftId;
        this.shift = id_shifts;
        this.consultoryId = id_consultories;
        this.startDayOfWeek = start_day_of_week;
        this.endDayOfWeek = end_day_of_week;
    }

    public Integer getConsultoryShiftId() {
        return consultoryShiftId;
    }

    public void setConsultoryShiftId(Integer consultoryShiftId) {
        this.consultoryShiftId = consultoryShiftId;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift id_shifts) {
        this.shift = id_shifts;
    }

    public Integer getConsultoryId() {
        return consultoryId;
    }

    public void setConsultoryId(Integer id_consultories) {
        this.consultoryId = id_consultories;
    }

    public Integer getStartDayOfWeek() {
        return startDayOfWeek;
    }

    public void setStartDayOfWeek(Integer start_day_of_week) {
        this.startDayOfWeek = start_day_of_week;
    }

    public Integer getEndDayOfWeek() {
        return endDayOfWeek;
    }

    public void setEndDayOfWeek(Integer end_day_of_week) {
        this.endDayOfWeek = end_day_of_week;
    }
}
