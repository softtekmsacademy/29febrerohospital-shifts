package com.softtek.msacademy.hospital.shiftsapi.service;

public interface WorkAreasService {

    boolean consultoryExists(int idConsultory);

    boolean laboratoryExists(int idLaboratory);
}
