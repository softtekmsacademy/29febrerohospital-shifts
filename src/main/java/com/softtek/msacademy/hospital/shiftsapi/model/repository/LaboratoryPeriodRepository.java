package com.softtek.msacademy.hospital.shiftsapi.model.repository;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.LaboratoryPeriod;
import com.softtek.msacademy.hospital.shiftsapi.model.entity.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaboratoryPeriodRepository extends JpaRepository<LaboratoryPeriod,Integer> {
    @Query("SELECT s FROM laboratories_shifts s where s.laboratoryId = :idlaboratories")
    List<LaboratoryPeriod> findPeriodsForLaboratory(@Param("idlaboratories") Integer idlaboratories);

    @Query("SELECT s FROM laboratories_shifts s where s.laboratoryId = :idlaboratories and s.shift = :idshift and s.startDayOfWeek = :startday " +
            "and s.endDayOfWeek = :endday")
    List<LaboratoryPeriod> findForCheck(@Param("idlaboratories") Integer idlaboratories, @Param("idshift") Shift idshift, @Param("startday") Integer startday, @Param("endday") Integer endday);
}
