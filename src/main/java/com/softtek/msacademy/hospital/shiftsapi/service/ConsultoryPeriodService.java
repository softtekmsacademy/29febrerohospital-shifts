package com.softtek.msacademy.hospital.shiftsapi.service;

import com.softtek.msacademy.hospital.shiftsapi.model.entity.ConsultoryPeriod;

import java.util.List;

public interface ConsultoryPeriodService {

    ConsultoryPeriod createConsultoryPeriod(ConsultoryPeriod consultoryPeriod);

    List<ConsultoryPeriod> getConsultoryPeriods();

    ConsultoryPeriod deleteConsultoryPeriod(ConsultoryPeriod consultoryPeriod);

    ConsultoryPeriod updateConsultoryPeriod(ConsultoryPeriod consultoryPeriod);

    ConsultoryPeriod patchConsultoryPeriod(int id, int shiftId, int startDay, int endDay);

    boolean exists(int id);

    List<ConsultoryPeriod> findForCheck(int idConsultory, int idShift, int startDay, int endDay);

    ConsultoryPeriod getConsultoryPeriod(int id);

    List<ConsultoryPeriod> findPeriodsForConsultory(int id);
}
